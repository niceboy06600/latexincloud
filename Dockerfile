FROM python:3.7.3

RUN pip install --upgrade pip

RUN pip install ansible

RUN ansible-galaxy collection install amazon.aws

RUN ansible-galaxy collection install community.aws

RUN ansible-galaxy collection install community.general

RUN pip install boto3
