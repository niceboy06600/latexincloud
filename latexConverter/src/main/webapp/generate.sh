cd /var/lib/tomcat10/webapps/latex/
rm math.*
decoded=`echo $1 | base64 --decode`
sed "s/##EXPRESSION##/${decoded}/g" < math_template.tex > math.tex
sed -i 's/\\x26/\&/g' math.tex
latex math.tex
dvipdfm math.dvi
pdfcrop.pl --margins "5 5 5 5" math.pdf
convert math-crop.pdf math.jpg
