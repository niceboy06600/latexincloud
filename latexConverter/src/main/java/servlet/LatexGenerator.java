package servlet;

import vdesoutter.calculator.calculatorlibrary.CalculatorNodeContainer;
import vdesoutter.calculator.calculatorlibrary.Operator.CalculatorNode;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;
import java.util.List;

public class LatexGenerator extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            CalculatorNodeContainer.getInstance().clearAll();
            byte[] decodedBytes = Base64.getDecoder().decode(request.getParameterValues("mathString")[0]);
            CalculatorNodeContainer.getInstance().insert(new String(decodedBytes));
            if (CalculatorNodeContainer.getInstance().getList().size() > 0) {
                String latexString = ((List<CalculatorNode>) CalculatorNodeContainer.getInstance().getList()).get(0).toLatex();
                response.setContentType("text/html");
                OutputStream out = response.getOutputStream();
                out.write(latexString.getBytes());
                out.close();
            }
        }
        catch (Exception ex) {
            OutputStream out = response.getOutputStream();
            out.write(ex.getMessage().getBytes());
            out.write(ex.getStackTrace().toString().getBytes());
            out.close();
        }
    }
}
