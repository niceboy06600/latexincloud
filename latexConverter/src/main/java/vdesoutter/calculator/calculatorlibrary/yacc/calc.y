%{
import java.lang.Math;
import java.io.*;
import java.util.StringTokenizer;
import java.util.List;
import java.util.ArrayList;
import vdesoutter.calculator.calculatorlibrary.Operator.*;
import vdesoutter.calculator.calculatorlibrary.CalculatorNodeContainer;
import vdesoutter.calculator.calculatorlibrary.computation.Pair;

import static java.lang.Character.isDigit;
%}

/* YACC Declarations */
%token NUM
%token CSTE
%token VAR
%token INV
%token INT
%token INT_2
%token INT_3
%token SUM
%token AVG
%token MIN
%token MAX
%token PROD
%token ID
%token SIN
%token COS
%token TAN
%token SINH
%token COSH
%token TANH
%token ASIN
%token ACOS
%token ATAN
%token ASINH
%token ACOSH
%token ATANH
%token LN
%token RE
%token IMG
%token ARG
%token MOD
%token ABS
%token SQRT
%token DER
%token DET
%token CONJ
%token TSP
%token TR
%token QUOTE
%token DELTA
%token NABLA
%token ROT
%token DIV
%token IF
%token THEN
%token ELSE
%token AND
%token OR
%token I_CPLX
%token GREATER_EQUAL
%token LOWER_EQUAL
%token GREATER
%token LOWER
%token EQUALITY
%token DOUBLE_QUOTE
%token DOT
%token FACT
%token NOT
%token NORM
%token RECUR
%token RAND
%token EQUA_DIFF
%left '-' '+'
%left '*' '/'
%left NEG /* negation--unary minus */
%right '^' FACT /* exponentiation */

/* Grammar follows */
%%
input: /* empty string */
 | input line { $$ = $2;}
 ;

line: '\n'
 | exp '\n' {
	 	      CalculatorNodeContainer.getInstance().add((CalculatorNode)$1.obj);
	 	      $$ = $1;
            }
 ;

exp: exp '+' exp {$$ = new CalculatorParserVal(OperatorValue.createAddComplex((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp '-' exp {$$ = new CalculatorParserVal(OperatorValue.createSubstComplex((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp '*' exp {$$ = new CalculatorParserVal(OperatorValue.createMultComplex((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp '/' exp {$$ = new CalculatorParserVal(new OperatorDivision((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp '^' exp {$$ = new CalculatorParserVal(new OperatorPower((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp '%' exp {$$ = new CalculatorParserVal(new OperatorModulo((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp '=' exp {$$ = new CalculatorParserVal(new OperatorAssign((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj));}
 | IF exp THEN exp ELSE exp { $$ = new CalculatorParserVal(new OperatorIf((CalculatorNode)$2.obj, (CalculatorNode) $4.obj, (CalculatorNode) $6.obj));}
 | exp GREATER exp {$$ = new CalculatorParserVal(new OperatorGreater((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp LOWER exp {$$ = new CalculatorParserVal(new OperatorLower((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp EQUALITY exp {$$ = new CalculatorParserVal(new OperatorEgality((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp LOWER_EQUAL exp {$$ = new CalculatorParserVal(new OperatorLowerEqual((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp GREATER_EQUAL exp {$$ = new CalculatorParserVal(new OperatorGreaterEqual((CalculatorNode) ($1.obj), (CalculatorNode) $3.obj)); }
 | exp AND exp {$$ = new CalculatorParserVal(new OperatorAnd((CalculatorNode) $1.obj, (CalculatorNode) $3.obj));}
 | NOT exp {$$ = new CalculatorParserVal(new OperatorNot((CalculatorNode) $2.obj));}
 | exp OR exp {$$ = new CalculatorParserVal(new OperatorOr((CalculatorNode) $1.obj, (CalculatorNode) $3.obj));}
 | exp DOT exp {$$ = new CalculatorParserVal(new OperatorScalarProduct((CalculatorNode) $1.obj, (CalculatorNode) $3.obj));}
 | exp FACT {$$ = new CalculatorParserVal(new OperatorFactoriel((CalculatorNode) $1.obj));}
 | '-' exp %prec NEG {if ((CalculatorNode) $2.obj instanceof OperatorValue) {
                        $$ = new CalculatorParserVal(new OperatorValue(-1.0 * ((OperatorValue) $2.obj).realPart, -1.0 * ((OperatorValue) $2.obj).imaginaryPart));
                      }
                      else {
                        $$ = new CalculatorParserVal(new OperatorMultiplication(new OperatorValue(-1.0), (CalculatorNode) $2.obj));
                      }
                      }
 | NUM {$$ = new CalculatorParserVal(new OperatorValue($1.dval)); }
 | CSTE {$$ = $1; }
 | I_CPLX {$$ = new CalculatorParserVal(new OperatorValue(0.0, 1.0)); }
 | VAR '(' args ')' {$$ = new CalculatorParserVal(new OperatorFunction($1.sval, (List<CalculatorNode>) $3.obj)); }
 | VAR {$$ = new CalculatorParserVal(new OperatorVariable($1.sval)); }
 | SIN '(' exp ')' {$$ = new CalculatorParserVal(new FunctionSinus((CalculatorNode) $3.obj)); }
 | INV '(' exp ')' {$$ = new CalculatorParserVal(new OperatorInversion((CalculatorNode) $3.obj)); }
 | AVG '(' args ')' {$$ = new CalculatorParserVal(new OperatorAverage((List<CalculatorNode>) $3.obj)); }
 | MIN '(' args ')' {$$ = new CalculatorParserVal(new OperatorMin((List<CalculatorNode>)$3.obj)); }
 | MAX '(' args ')' {$$ = new CalculatorParserVal(new OperatorMax((List<CalculatorNode>)$3.obj)); }
 | COS '(' exp ')' {$$ = new CalculatorParserVal(new FunctionCosinus((CalculatorNode) $3.obj)); }
 | TAN '(' exp ')' {$$ = new CalculatorParserVal(new FunctionTangente((CalculatorNode) $3.obj)); }
 | RE '(' exp ')' {$$ = new CalculatorParserVal(new FunctionRealPart((CalculatorNode) $3.obj)); }
 | IMG '(' exp ')' {$$ = new CalculatorParserVal(new FunctionImaginaryPart((CalculatorNode) $3.obj)); }
 | CONJ '(' exp ')' {$$ = new CalculatorParserVal(new FunctionCplxConj((CalculatorNode) $3.obj)); }
 | ARG '(' exp ')' {$$ = new CalculatorParserVal(new FunctionArgument((CalculatorNode) $3.obj)); }
 | MOD '(' exp ')' {$$ = new CalculatorParserVal(new FunctionModule((CalculatorNode) $3.obj)); }
 | SINH '(' exp ')' {$$ = new CalculatorParserVal(new FunctionSinusHyper((CalculatorNode) $3.obj)); }
 | COSH '(' exp ')' {$$ = new CalculatorParserVal(new FunctionCosinusHyper((CalculatorNode) $3.obj)); }
 | TANH '(' exp ')' {$$ = new CalculatorParserVal(new FunctionTangenteHyper((CalculatorNode) $3.obj)); }
 | ASIN '(' exp ')' {$$ = new CalculatorParserVal(new FunctionArcSinus((CalculatorNode) $3.obj)); }
 | ACOS '(' exp ')' {$$ = new CalculatorParserVal(new FunctionArcCosinus((CalculatorNode) $3.obj)); }
 | ATAN '(' exp ')' {$$ = new CalculatorParserVal(new FunctionArcTangente((CalculatorNode) $3.obj)); }
 | ASINH '(' exp ')' {$$ = new CalculatorParserVal(new FunctionArcSinusHyper((CalculatorNode) $3.obj)); }
 | ACOSH '(' exp ')' {$$ = new CalculatorParserVal(new FunctionArcCosinusHyper((CalculatorNode) $3.obj)); }
 | ATANH '(' exp ')' {$$ = new CalculatorParserVal(new FunctionArcTangenteHyper((CalculatorNode) $3.obj)); }
 | RAND '(' exp ',' exp ')' {$$ = new CalculatorParserVal(new OperatorRandom((CalculatorNode) $3.obj, (CalculatorNode) $5.obj)); }
 | ROT VAR '(' args ')' {$$ = new CalculatorParserVal(new OperatorRotationel(new OperatorVariable($2.sval), (List<CalculatorNode>) $4.obj)); }
 | DIV VAR '(' args ')' {$$ = new CalculatorParserVal(new OperatorDivergence(new OperatorVariable($2.sval), (List<CalculatorNode>) $4.obj)); }
 | TR '(' exp ')' {$$ = new CalculatorParserVal(new OperatorTrace((CalculatorNode) $3.obj)); }
 | DELTA VAR '(' args ')' {$$ = new CalculatorParserVal(new OperatorLaplacien(new OperatorVariable ($2.sval), (List<CalculatorNode>) $4.obj)); }
 | NABLA VAR '(' args ')' {$$ = new CalculatorParserVal(new OperatorGradient(new OperatorVariable ($2.sval), (List<CalculatorNode>) $4.obj)); }
 | ID '(' exp ')' {$$ = new CalculatorParserVal(new OperatorIdentity((CalculatorNode) $3.obj)); }
 | TSP '(' exp ')' {$$ = new CalculatorParserVal(new OperatorTransposee((CalculatorNode) $3.obj)); }
 | DET '(' exp ')' {$$ = new CalculatorParserVal(new OperatorDeterminant((CalculatorNode) $3.obj)); }
 | LN '(' exp ')' {$$ = new CalculatorParserVal(new FunctionLogarithme((CalculatorNode) $3.obj)); }
 | SQRT '(' exp ')' {$$ = new CalculatorParserVal(new FunctionSquareRoot((CalculatorNode) $3.obj)); }
 | ABS '(' exp ')' {$$ = new CalculatorParserVal(new FunctionAbsolute((CalculatorNode) $3.obj)); }
 | '(' exp ')' {$$ = new CalculatorParserVal(new OperatorParenthesis((CalculatorNode) $2.obj)); }
 | VAR QUOTE '(' args ')' {$$ = new CalculatorParserVal(new OperatorDerivate(new OperatorVariable($1.sval), 1, (List<CalculatorNode>) $4.obj)); }
 | VAR DOUBLE_QUOTE '(' args ')' {$$ = new CalculatorParserVal(new OperatorDerivate(new OperatorVariable($1.sval), 2, (List<CalculatorNode>) $4.obj)); }
 | DER VAR '/' DER VAR '(' args ')' {ArrayList<Pair<OperatorVariable, Integer> > derArg = new ArrayList<Pair<OperatorVariable, Integer> > (); derArg.add(new Pair<OperatorVariable, Integer> (new OperatorVariable($5.sval), 1)); $$ = new CalculatorParserVal(new OperatorDerivate(new OperatorVariable($2.sval), derArg, (List<CalculatorNode>) $7.obj)); }
 | DER '^' NUM VAR '/' der_args '(' args ')' {$$ = new CalculatorParserVal(new OperatorDerivate(new OperatorVariable($4.sval), (ArrayList<Pair<OperatorVariable, Integer> >)$6.obj, (List<CalculatorNode>) $8.obj)); }
 | SUM '(' exp ',' VAR ',' exp ',' exp ')' {$$ = new CalculatorParserVal(new OperatorSummation((CalculatorNode) $3.obj, new OperatorVariable($5.sval), (CalculatorNode) $7.obj, (CalculatorNode) $9.obj)); }
 | PROD '(' exp ',' VAR ',' exp ',' exp ')' {$$ = new CalculatorParserVal(new OperatorProduct((CalculatorNode) $3.obj, new OperatorVariable($5.sval), (CalculatorNode) $7.obj, (CalculatorNode) $9.obj)); }
 | INT '(' exp ',' VAR ',' exp ',' exp ')' {List<OperatorVariable> variables = new ArrayList<OperatorVariable> ();
                                            List<CalculatorNode> minBound = new ArrayList<CalculatorNode> ();
                                            List<CalculatorNode> maxBound = new ArrayList<CalculatorNode> ();
                                            variables.add(new OperatorVariable($5.sval));
                                            minBound.add((CalculatorNode)$7.obj);
                                            maxBound.add((CalculatorNode)$9.obj);
                                            $$ = new CalculatorParserVal(new OperatorIntegration((CalculatorNode) $3.obj, variables, minBound, maxBound));  }
 | INT_2 '(' exp ',' '(' VAR ',' VAR ')' ',' '(' exp ',' exp ')' ',' '(' exp ',' exp ')' ')' {
                                             List<OperatorVariable> variables = new ArrayList<OperatorVariable> ();
                                             List<CalculatorNode> minBound = new ArrayList<CalculatorNode> ();
                                             List<CalculatorNode> maxBound = new ArrayList<CalculatorNode> ();
                                             variables.add(new OperatorVariable($6.sval));
                                             variables.add(new OperatorVariable($8.sval));
                                             minBound.add((CalculatorNode)$12.obj);
                                             minBound.add((CalculatorNode)$14.obj);
                                             maxBound.add((CalculatorNode)$18.obj);
                                             maxBound.add((CalculatorNode)$20.obj);
                                             $$ = new CalculatorParserVal(new OperatorIntegration((CalculatorNode) $3.obj, variables, minBound, maxBound));  }
 | INT_3 '(' exp ',' '(' VAR ',' VAR ',' VAR ')' ',' '(' exp ',' exp ',' exp ')' ',' '(' exp ',' exp ',' exp ')' ')' {
                                             List<OperatorVariable> variables = new ArrayList<OperatorVariable> ();
                                             List<CalculatorNode> minBound = new ArrayList<CalculatorNode> ();
                                             List<CalculatorNode> maxBound = new ArrayList<CalculatorNode> ();
                                             variables.add(new OperatorVariable($6.sval));
                                             variables.add(new OperatorVariable($8.sval));
                                             variables.add(new OperatorVariable($10.sval));
                                             minBound.add((CalculatorNode)$14.obj);
                                             minBound.add((CalculatorNode)$16.obj);
                                             minBound.add((CalculatorNode)$18.obj);
                                             maxBound.add((CalculatorNode)$22.obj);
                                             maxBound.add((CalculatorNode)$24.obj);
                                             maxBound.add((CalculatorNode)$26.obj);
                                             $$ = new CalculatorParserVal(new OperatorIntegration((CalculatorNode) $3.obj, variables, minBound, maxBound));   }
 | NORM '(' vector ',' exp ')' { $$ = new CalculatorParserVal(new OperatorNorme((CalculatorNode) $3.obj, (CalculatorNode) $5.obj));}
 | vector { $$ = $1; }
 | RECUR '(' exp ',' VAR ','  '(' init_values ')' ')' { $$ = new CalculatorParserVal(new OperatorRecurrence((CalculatorNode) $3.obj,new OperatorVariable($5.sval), (List<CalculatorNode>)$8.obj)); }
;

eq_diff: VAR QUOTE '(' args ')' '=' exp  {$$ = new CalculatorParserVal(new OperatorAssign(new OperatorDerivate(new OperatorVariable($1.sval), 1, (List<CalculatorNode>) $4.obj), (CalculatorNode) $7.obj)); }
 | VAR DOUBLE_QUOTE '(' args ')' '=' exp  {$$ = new CalculatorParserVal(new OperatorAssign(new OperatorDerivate(new OperatorVariable($1.sval), 2, (List<CalculatorNode>) $4.obj), (CalculatorNode) $7.obj)); }

assignment: function '=' exp {$$ = new CalculatorParserVal(new OperatorAssign((CalculatorNode) $1.obj, ((CalculatorNode) $3.obj)));}
;

der_init: der_init_assign {List<CalculatorNode> temp = new ArrayList<CalculatorNode>(); temp.add((CalculatorNode) $1.obj); $$=new CalculatorParserVal(temp);}
| der_init ',' der_init_assign {List<CalculatorNode> temp = (List<CalculatorNode>) $1.obj; temp.add((CalculatorNode) $3.obj); $$ = new CalculatorParserVal(temp);}
;

der_init_assign: der_init_val '=' exp {$$ = new CalculatorParserVal(new OperatorAssign((CalculatorNode) $1.obj, ((CalculatorNode) $3.obj)));}
;

der_init_val: VAR '(' args ')' {$$ = new CalculatorParserVal(new OperatorFunction($1.sval, (List<CalculatorNode>) $3.obj)); }
 | VAR QUOTE '(' args ')' {$$ = new CalculatorParserVal(new OperatorDerivate(new OperatorVariable($1.sval), 1, (List<CalculatorNode>) $4.obj)); }
;

function: VAR '(' args ')' {$$ = new CalculatorParserVal(new OperatorFunction($1.sval, (List<CalculatorNode>) $3.obj)); }
 | VAR {$$ = new CalculatorParserVal(new OperatorVariable($1.sval)); }
;

args: exp {List<CalculatorNode> temp = new ArrayList<CalculatorNode>(); temp.add((CalculatorNode) $1.obj); $$=new CalculatorParserVal(temp);}
| args ',' exp {List<CalculatorNode> temp = (List<CalculatorNode>) $1.obj; temp.add((CalculatorNode) $3.obj); $$ = new CalculatorParserVal(temp);}
;

init_values: assignment {List<CalculatorNode> temp = new ArrayList<CalculatorNode>(); temp.add((CalculatorNode) $1.obj); $$=new CalculatorParserVal(temp);}
| init_values ',' assignment {List<CalculatorNode> temp = (List<CalculatorNode>) $1.obj; temp.add((CalculatorNode) $3.obj); $$ = new CalculatorParserVal(temp);}
;

vect_args: exp {List<CalculatorNode> values = new ArrayList<>(); values.add((CalculatorNode)$1.obj);$$=new CalculatorParserVal(values);}
| vect_args ',' exp {((List<CalculatorNode>)$1.obj).add((CalculatorNode)$3.obj); $$=new CalculatorParserVal($1.obj);}
;

vector: '[' vect_args ']' {$$ = new CalculatorParserVal(new OperatorVector((List<CalculatorNode>)$2.obj)); }
;

der_arg: DER VAR '^' NUM { $$ = new CalculatorParserVal(new Pair<OperatorVariable, Integer> (new OperatorVariable($2.sval), (int)($4.dval)));}
| DER VAR {$$ = new CalculatorParserVal(new Pair<OperatorVariable, Integer> (new OperatorVariable($2.sval), 1));}

der_args: der_args der_arg {ArrayList<Pair<OperatorVariable, Integer> > derArg = (ArrayList<Pair<OperatorVariable, Integer> >) $1.obj; derArg.add((Pair<OperatorVariable, Integer>)$2.obj); $$ = new CalculatorParserVal(derArg);}
| der_arg { ArrayList<Pair<OperatorVariable, Integer> > derArg = new ArrayList<Pair<OperatorVariable, Integer> > (); derArg.add((Pair<OperatorVariable, Integer>) $1.obj); $$ = new CalculatorParserVal(derArg);}

%%

String ins;
StringTokenizer st;
Boolean insertValue;

void yyerror(String s) throws Exception
{
   throw new Exception("Parsing error: " + s);
}

boolean newline;
int yylex()
{
String s;
int tok;
Double d;
 //System.out.print("yylex ");
 if (!st.hasMoreTokens())
 if (!newline)
 {
 newline=true;
 return '\n'; //So we look like classic YACC example
 }
 else
 return 0;
 s = st.nextToken();
 //Log.warning("tok:"+s);
 try
 {
 d = Double.valueOf(s);/*this may fail*/
 yylval = new CalculatorParserVal(d.doubleValue()); //SEE BELOW
 tok = NUM;
 }
 catch (Exception e)
 {
   if (s.equals("sin")) {
      tok = SIN;
   }
   else if (s.equals("cos")) {
      tok = COS;
   }
   else if (s.equals("tan")) {
      tok = TAN;
   }
   else if (s.equals("asin")) {
      tok = ASIN;
   }
   else if (s.equals("acos")) {
      tok = ACOS;
   }
   else if (s.equals("atan")) {
      tok = ATAN;
   }
   else if (s.equals("asinh")) {
      tok = ASINH;
   }
   else if (s.equals("acosh")) {
      tok = ACOSH;
   }
   else if (s.equals("atanh")) {
      tok = ATANH;
   }
   else if (s.equals("sinh")) {
      tok = SINH;
   }
   else if (s.equals("cosh")) {
      tok = COSH;
   }
   else if (s.equals("tanh")) {
      tok = TANH;
   }
   else if (s.equals("min")) {
      tok = MIN;
   }
   else if (s.equals("avg")) {
      tok = AVG;
   }
   else if (s.equals("max")) {
      tok = MAX;
   }
   else if (s.equals("rand")) {
      tok = RAND;
   }   
   else if (s.equals("inv")) {
      tok = INV;
   }
   else if (s.equals("det")) {
      tok = DET;
   }
   else if (s.equals("tr")) {
      tok = TR;
   }
   else if (s.equals("Id")) {
      tok = ID;
   }
   else if (s.equals("tsp")) {
      tok = TSP;
   }
   else if (s.equals("and")) {
      tok = AND;
   }
   else if (s.equals("or")) {
      tok = OR;
   }
   else if (s.equals("not")) {
      tok = NOT;   
   }
   else if (s.equals("rot")) {
      tok = ROT;
   }
   else if (s.equals("div")) {
      tok = DIV;
   }
   else if (s.equals("norm")) {
      tok = NORM;
   }
   else if (s.equals("equa_diff")) {
      tok = EQUA_DIFF;
   }
   else if (s.equals("\u221E") || s.equals("infinity")) { // Symbole of Inifinity
      yylval = new CalculatorParserVal(new OperatorConstante(OperatorConstante.Constante.Infinity));
      tok = CSTE;
   }
   else if (s.equals("\u03C0") || s.equals("pi")) { // Symbole of Pi
      yylval = new CalculatorParserVal(new OperatorConstante(OperatorConstante.Constante.Pi));
      tok = CSTE;
   }
   else if (s.equals("!")) {
      tok = FACT;
   }
   else if (s.equals("e")) {
      yylval = new CalculatorParserVal(new OperatorConstante(OperatorConstante.Constante.E));
      tok = CSTE;
   }
   else if (s.equals("i")) {
      tok = I_CPLX;
   }
   else if (s.equals("ln")) {
      tok = LN;
   }
   else if (s.equals("Re")) {
      tok = RE;
   }
   else if (s.equals("conj")) {
      tok = CONJ;
   }
   else if (s.equals("Im")) {
      tok = IMG;
   }
   else if (s.equals("arg")) {
      tok = ARG;
   }
   else if (s.equals("mod")) {
      tok = MOD;
   }
   else if (s.equals("\u2265") || s.equals(">=")) { // Symbole Greater or equal
      tok = GREATER_EQUAL;
   }
   else if (s.equals("\u2264") || s.equals("<=")) { // Symbole Lower or equal
      tok = LOWER_EQUAL;
   }
   else if (s.equals(">")) {
      tok = GREATER;
   }
   else if (s.equals("<")) {
      tok = LOWER;
   }
   else if (s.equals("==")) {
      tok = EQUALITY;
   }
   else if (s.equals("if")) {
      tok = IF;
   }
   else if (s.equals("then")) {
      tok = THEN;
   }
   else if (s.equals("else")) {
      tok = ELSE;
   }
   else if (s.equals("\u221A") || s.equals("sqrt")) { // Symbole racine carre
      tok = SQRT;
   }
   else if (s.equals("abs")) {
      tok = ABS;
   }
   else if (s.equals("+")) {
      tok = s.charAt(0);
   }
   else if (s.equals("-")) {
      tok = s.charAt(0);
   }
   else if (s.equals("/")) {
      tok = s.charAt(0);
   }
   else if (s.equals("*")) {
      tok = s.charAt(0);
   }
   else if (s.equals("%")) {
      tok = s.charAt(0);
   }
   else if (s.equals(",")) {
      tok = s.charAt(0);
   }
   else if (s.equals("^")) {
      tok = s.charAt(0);
   }
   else if (s.equals("(")) {
      tok = s.charAt(0);
   }
   else if (s.equals(")")) {
      tok = s.charAt(0);
   }
   else if (s.equals("=")) {
      tok = s.charAt(0);
   }
   else if (s.equals("[")) {
      tok = s.charAt(0);
   }
   else if (s.equals("]")) {
      tok = s.charAt(0);
   }
   else if (s.equals("'")) {
      tok = QUOTE;
   }
   else if (s.equals("\"")) {
      tok = DOUBLE_QUOTE;
   }
   else if (s.equals("recur")) {
      tok = RECUR;
   }
   else if (s.equals("\u2202") || s.equals("d")) { // Symbole derivation
      tok = DER;
   }
   else if (s.equals("\u222B") || s.equals("Int1")) { // Symbole integrale
      tok = INT;
   }
   else if (s.equals("\u222C") || s.equals("Int2")) { // Symbole integrale double
      tok = INT_2;
   }
   else if (s.equals("\u222D") || s.equals("Int3")) { // Symbole integrale triple
      tok = INT_3;
   }
   else if (s.equals("\u2211") || s.equals("Sum")) { // Symbole Somme (Sigma)
      tok = SUM;
   }
   else if (s.equals("\u03A0") || s.equals("Prod")) { // Symbole Produit (pi)
      tok = PROD;
   }
   else if (s.equals("\u0394") || s.equals("lapl")) { // Symbole Delta (Laplacien)
      tok = DELTA;
   }
   else if (s.equals("\u2207") || s.equals("grad")) { // Symbole Nable (Gradient)
      tok = NABLA;
   }
   else if (s.equals(".")) { // scalar product
      tok = DOT;
   }
   else {
      yylval = new CalculatorParserVal(s);
      tok = VAR;
   }
 }
 return tok;
}

public CalculatorNode parse(String input, Boolean insert) throws Exception
{
    insertValue = insert;
	input = input.replaceAll("\\[", " [ ");
	input = input.replaceAll("\\]", " ] ");
	input = input.replaceAll("\\(", " ( ");
	input = input.replaceAll("=", " = ");
	input = input.replaceAll("<", " < ");
	input = input.replaceAll(">", " > ");
	input = input.replaceAll("=  =", "==");
	input = input.replaceAll(">  =", ">=");
	input = input.replaceAll("<  =", "<=");
	input = input.replaceAll("\\)", " ) ");
	input = input.replaceAll("\\+", " + ");
	input = input.replaceAll("-", " - ");
	input = input.replaceAll("\\*", " * ");
	input = input.replaceAll("/", " / ");
	input = input.replaceAll("!", " ! ");
	input = input.replaceAll(",", " , ");
	input = input.replaceAll("'", " ' ");
	input = input.replaceAll("\"", " \" ");
	input = input.replaceAll("\\^", " ^ ");
	input = input.replaceAll("%", " % ");
	input = input.replaceAll("\u2202", " \u2202 ");
	input = input.replaceAll("\u222B", " \u222B ");
	input = input.replaceAll("\u222C", " \u222C ");
	input = input.replaceAll("\u222D", " \u222D ");
	input = input.replaceAll("\u2211", " \u2211 ");
	input = input.replaceAll("\u03A0", " \u03A0 ");
	input = input.replaceAll("\u0394", " \u0394 ");
	input = input.replaceAll("\u2207", " \u2207 ");
	input = input.replaceAll("\u2265", " \u2265 ");
	input = input.replaceAll("\u2264", " \u2264 ");
    // Check the dots in the string and if not around digits, add spaces
    for (int charIdx = 1; charIdx < input.length() - 1; ++ charIdx) {
      if (input.charAt(charIdx) == '.') {
        if (isDigit(input.charAt(charIdx-1)) && isDigit(input.charAt(charIdx+1))) {

        }
        else {
          input = input.substring(0, charIdx) + " . " + input.substring(charIdx + 1);
          charIdx += 2;
        }
      }
    }
 InputStream is = new ByteArrayInputStream(input.getBytes());
 BufferedReader in = new BufferedReader(new InputStreamReader(is));
 try
 {
 ins = in.readLine();
 }
 catch (Exception e)
 {
 throw e;
 }
 st = new StringTokenizer(ins);
 newline=false;
 yyparse();
 return ((CalculatorNode) yyval.obj);
}
