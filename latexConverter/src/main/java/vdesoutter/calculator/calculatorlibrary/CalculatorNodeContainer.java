package vdesoutter.calculator.calculatorlibrary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import vdesoutter.calculator.calculatorlibrary.Operator.CalculatorNode;
import vdesoutter.calculator.calculatorlibrary.Operator.OperatorAssign;
import vdesoutter.calculator.calculatorlibrary.Operator.OperatorEqual;
import vdesoutter.calculator.calculatorlibrary.Operator.OperatorFunction;
import vdesoutter.calculator.calculatorlibrary.Operator.OperatorVariable;
import vdesoutter.calculator.calculatorlibrary.computation.Pair;
import vdesoutter.calculator.calculatorlibrary.yacc.CalculatorParser;


public class CalculatorNodeContainer<Bitmap> {

    private static CalculatorNodeContainer INSTANCE = new CalculatorNodeContainer<>();
    private List<CalculatorNode> list;

    private CalculatorNodeContainer() {
        list = new ArrayList<CalculatorNode>();
    }

    public static CalculatorNodeContainer getInstance() {
        return INSTANCE;
    }

    public List<CalculatorNode> getList() {
        return list;
    }

    public void add(CalculatorNode func) throws Exception {
       list.add(func);
    }

    public void clearAll() {
        list.clear();
    }

    public void insert(String value) throws Exception {
        CalculatorParser parser = new CalculatorParser();
        if (value.length() == 0) {
            return;
        }
        try {
            parser.parse(value, true);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
}
