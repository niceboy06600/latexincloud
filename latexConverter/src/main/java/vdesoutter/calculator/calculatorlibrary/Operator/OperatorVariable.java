package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class OperatorVariable extends CalculatorNode {

    public String value;

    public OperatorVariable(String value) {
        this.value = value;
    }

    @Override
    public String print() {
        return value;
    }

    @Override
    public String toLatex() throws Exception {
        return value;
    }
}
