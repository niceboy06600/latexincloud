package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

import vdesoutter.calculator.calculatorlibrary.CalculatorNodeContainer;

public class FunctionSinus extends CalculatorNode {
    public CalculatorNode value;

    public FunctionSinus(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "sin(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\sin\\left(" + value.toLatex() + "\\right)";
    }

}
