package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionArcTangente extends CalculatorNode {
    public CalculatorNode value;

    public FunctionArcTangente(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "atan(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\arctan\\left(" + value.toLatex() + "\\right)";
    }

}
