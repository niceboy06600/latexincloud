package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionArcCosinusHyper extends CalculatorNode {
    public CalculatorNode value;

    public FunctionArcCosinusHyper(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "acosh(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "acosh\\left(" + value.toLatex() + "\\right)";
    }
}
