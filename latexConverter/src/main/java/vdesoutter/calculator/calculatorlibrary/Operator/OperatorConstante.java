package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class OperatorConstante extends CalculatorNode {

    Constante value;

    public OperatorConstante(Constante value) {
        this.value = value;
    }

    @Override
    public String print() {
        if (value == Constante.E) {
            return "e";
        }
        if (value == Constante.Pi) {
            return "\u03C0";
        }
        if (value == Constante.Infinity) {
            return "\u221E";
        }
        return "";
    }

    @Override
    public String toLatex() throws Exception {
        if (value == Constante.E) {
            return "e";
        }
        if (value == Constante.Pi) {
            return "\\pi";
        }
        if (value == Constante.Infinity) {
            return "\\infty";
        }
        return "";
    }

    public enum Constante {
        Pi, E, Infinity
    }
}
