package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorModulo extends CalculatorNode {

    public CalculatorNode firstArg;
    public CalculatorNode secondArg;

    public OperatorModulo(CalculatorNode firstArg, CalculatorNode secondArg) {
        super();
        this.firstArg = firstArg;
        this.secondArg = secondArg;
    }

    @Override
    public String print() throws Exception {
        String res = "";
        res += firstArg.print();
        res += "%";
        res += secondArg.print();
        return res;
    }

    @Override
    public String toLatex() throws Exception {
        String res = "";
        res += firstArg.toLatex();
        res += "%";
        res += secondArg.toLatex();
        return res;
    }
}
