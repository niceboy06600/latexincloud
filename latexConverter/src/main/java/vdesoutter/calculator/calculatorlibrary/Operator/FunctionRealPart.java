package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class FunctionRealPart extends CalculatorNode {
    public CalculatorNode value;

    public FunctionRealPart(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "Re(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "Re\\left(" + value.toLatex() + "\\right)";
    }

}
