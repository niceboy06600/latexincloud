package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OperatorNorme extends CalculatorNode {

    public CalculatorNode expr1;
    public CalculatorNode expr2;

    public OperatorNorme(CalculatorNode expr1, CalculatorNode expr2) {
        super();
        this.expr1 = expr1;
        this.expr2 = expr2;
    }

    @Override
    public String print() throws Exception {
        return "norm(" + this.expr1.print() + ", " + this.expr2.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\left\\Vert" + this.expr1.toLatex() + "\\right\\Vert_{" + this.expr2.toLatex() + "}";
    }
}
