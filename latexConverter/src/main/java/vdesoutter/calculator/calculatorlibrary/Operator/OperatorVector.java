package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import vdesoutter.calculator.calculatorlibrary.Matrix.MatrixSize;

public class OperatorVector extends CalculatorNode {

    public List<CalculatorNode> values;
    public Boolean isMatrix = false;
    public Boolean isTransposed = false;
    public MatrixSize size;

    public OperatorVector(List<CalculatorNode> values) throws Exception {
        this.values = values;
        if (values.get(0).getClass() == OperatorVector.class) {
            isMatrix = true;
            int size0 = ((OperatorVector) values.get(0)).values.size();
            // Check if all the vectors have the same size ..
            for (int idx = 1; idx < this.values.size(); ++idx) {
                if (((OperatorVector) values.get(idx)).values.size() != size0) {
                    throw new Exception("Matrix not valid");
                }
            }
            size = new MatrixSize(values.size(), size0);
        } else {
            isMatrix = false;
            size = new MatrixSize(1, values.size());
        }
    }

    public CalculatorNode get(int idxX, int idxY) throws Exception {
        if (isMatrix) {
            if ((idxX < 0) || (idxX >= size.sizeX)) {
                throw new Exception("Getting out of range realPart X (" + idxX + " vs. " + size.sizeX + ")");
            }
            if ((idxY < 0) || (idxY >= size.sizeY)) {
                throw new Exception("Getting out of range realPart Y (" + idxY + " vs. " + size.sizeY + ")");
            }
            return ((OperatorVector) values.get(idxX)).values.get(idxY);
        } else {
            if ((idxX != 0) && (idxY != 0)) {
                throw new Exception("This is a vector");
            } else if (idxX == 0) {
                return values.get(idxY);
            } else if (idxY == 0) {
                return values.get(idxX);
            } else {
                throw new Exception("Should not to be here!");
            }
        }
    }

    @Override
    public String print() throws Exception {
        String printValue = "";
        if (isTransposed) {
            printValue += "tsp(";
        }
        printValue += "[";
        for (int idx = 0; idx < values.size(); ++idx) {
            if (idx != 0) {
                printValue += ",";
            }
            printValue += values.get(idx).print();
        }
        printValue += "]";
        if (isTransposed) {
            printValue += ")";
        }
        return printValue;
    }

    @Override
    public String toLatex() throws Exception {
        String printValue = "";
        if (isTransposed) {
            printValue += "^t";
        }
        if (isMatrix) {
            printValue += "\\begin{pmatrix}";
            for (int idxY = 0; idxY < size.sizeY; ++idxY) {
                for (int idxX = 0; idxX < size.sizeX; ++idxX) {
                    printValue += this.get(idxX, idxY).toLatex();
                    if (idxX != size.sizeX - 1) {
                        printValue += " \\x26 ";
                    }
                }
                if (idxY != size.sizeY - 1) {
                    printValue += " \\\\ ";
                }
            }
            printValue += "\\end{pmatrix}";
        } else {
            printValue += "\\begin{bmatrix}";
            for (int idx = 0; idx < size.sizeY; ++idx) {
                if (idx != 0) {
                    printValue += " \\\\ ";
                }
                printValue += values.get(idx).toLatex();
            }
            printValue += "\\end{bmatrix}";
        }
        return printValue;
    }
}
