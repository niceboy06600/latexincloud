package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorTrace extends CalculatorNode {

    public CalculatorNode value;

    public OperatorTrace(CalculatorNode firstArg) {
        super();
        this.value = firstArg;
    }

    @Override
    public String print() throws Exception {
        return "tr(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "tr" + value.toLatex() + "";
    }
}
