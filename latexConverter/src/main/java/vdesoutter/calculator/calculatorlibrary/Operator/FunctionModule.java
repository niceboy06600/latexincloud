package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class FunctionModule extends CalculatorNode {
    public CalculatorNode value;

    public FunctionModule(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "mod(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "mod\\left(" + value.toLatex() + "\\right)";
    }
}
