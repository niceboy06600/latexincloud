package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionCosinus extends CalculatorNode {
    public CalculatorNode value;

    public FunctionCosinus(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "cos(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\cos\\left(" + value.toLatex() + "\\right)";
    }

}
