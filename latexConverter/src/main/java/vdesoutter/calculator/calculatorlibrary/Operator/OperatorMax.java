package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OperatorMax extends CalculatorNode {

    public List<CalculatorNode> values;

    public OperatorMax(List<CalculatorNode> secondArg) {
        super();
        this.values = secondArg;
    }

    @Override
    public String print() throws Exception {
        String res = "max(";
        for (int curArgIdx = 0; curArgIdx < values.size(); ++curArgIdx) {
            if (curArgIdx != 0) {
                res += ",";
            }
            res += values.get(curArgIdx).print();
        }
        res += ")";

        return res;
    }

    @Override
    public String toLatex() throws Exception {
        String res = "\\max\\left(";
        for (int curArgIdx = 0; curArgIdx < values.size(); ++curArgIdx) {
            if (curArgIdx != 0) {
                res += ",";
            }
            res += values.get(curArgIdx).print();
        }
        res += "\\right)";

        return res;
    }
}
