package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorTransposee extends CalculatorNode {

    public CalculatorNode firstArg;

    public OperatorTransposee(CalculatorNode firstArg) {
        super();
        this.firstArg = firstArg;
    }

    @Override
    public String print() throws Exception {
        return "tsp(" + firstArg.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "^t" + firstArg.toLatex() + "";
    }
}
