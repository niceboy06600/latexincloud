package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorInversion extends CalculatorNode {

    public CalculatorNode firstArg;

    public OperatorInversion(CalculatorNode firstArg) {
        super();
        this.firstArg = firstArg;
    }

    @Override
    public String print() throws Exception {
        return "inv(" + firstArg.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return firstArg.toLatex() + "^{-1}";
    }
}
