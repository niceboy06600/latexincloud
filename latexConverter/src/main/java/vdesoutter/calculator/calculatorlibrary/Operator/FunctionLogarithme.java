package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionLogarithme extends CalculatorNode {
    public CalculatorNode value;

    public FunctionLogarithme(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "ln(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\ln\\left(" + value.toLatex() + "\\right)";
    }

}
