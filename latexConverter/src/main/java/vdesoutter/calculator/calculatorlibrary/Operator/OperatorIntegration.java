package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OperatorIntegration extends CalculatorNode {

    public CalculatorNode function;
    public List<CalculatorNode> minBound;
    public List<CalculatorNode> maxBound;
    public List<OperatorVariable> variables;

    public OperatorIntegration(CalculatorNode function, List<OperatorVariable> variables, List<CalculatorNode> minBound, List<CalculatorNode> maxBound) {
        super();
        this.function = function;
        this.variables = variables;
        this.maxBound = maxBound;
        this.minBound = minBound;
    }

    @Override
    public String print() throws Exception {
        // integrale simple : \u222B
        if (this.variables.size() == 1) {
            return "\u222B(" + this.function.print() + "," +
                    this.variables.get(0).value + "," +
                    this.minBound.get(0).print() + "," +
                    this.maxBound.get(0).print() + ")";
        }

        // integrale double : \u222C
        if (this.variables.size() == 2) {
            return "\u222C(" + this.function.print() + ",(" +
                    this.variables.get(0).value + "," +
                    this.variables.get(1).value + "),(" +
                    this.minBound.get(0).print() + "," +
                    this.minBound.get(1).print() + "),(" +
                    this.maxBound.get(0).print() + "," +
                    this.maxBound.get(1).print() + "))";
        }

        // integrale triple : \u222D
        if (this.variables.size() == 3) {
            return "\u222D(" + this.function.print() + ",(" +
                    this.variables.get(0).value + "," +
                    this.variables.get(1).value + "," +
                    this.variables.get(2).value + "),(" +
                    this.minBound.get(0).print() + "," +
                    this.minBound.get(1).print() + "," +
                    this.minBound.get(2).print() + "),(" +
                    this.maxBound.get(0).print() + "," +
                    this.maxBound.get(1).print() + "," +
                    this.maxBound.get(2).print() + "))";
        }
        return "";
    }

    @Override
    public String toLatex() throws Exception {
        String latexString = "";
        for (int idx = 0; idx < this.variables.size(); idx++) {
            latexString += "\\int_{" + this.variables.get(idx).toLatex() + "=" + this.minBound.get(idx).toLatex() + "}^{" +
                    this.variables.get(idx).toLatex() + "=" + this.maxBound.get(idx).toLatex() + "}";
        }
        latexString += this.function.toLatex();
        for (int idx = 0; idx < this.variables.size(); idx++) {
            latexString += "\\;\\mathrm{d} " + this.variables.get(idx).toLatex();
        }
        return latexString;
    }
}
