package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class OperatorBoolean extends CalculatorNode {

    public Boolean value;

    OperatorBoolean(boolean value) {
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return this.value.toString();
    }

    @Override
    public String toLatex() throws Exception {
        return this.value.toString();
    }

}
