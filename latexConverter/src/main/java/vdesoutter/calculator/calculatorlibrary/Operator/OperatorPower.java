package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorPower extends CalculatorNode {

    public CalculatorNode firstArg;
    public CalculatorNode secondArg;

    public OperatorPower(CalculatorNode firstArg, CalculatorNode secondArg) {
        super();
        this.firstArg = firstArg;
        this.secondArg = secondArg;
    }

    @Override
    public String print() throws Exception {
        String res = "";
        res += firstArg.print();
        res += "^";
        res += secondArg.print();
        return res;
    }

    @Override
    public String toLatex() throws Exception {
        String res = "";
            res += firstArg.toLatex();
        if ((this.firstArg.getClass() == OperatorVector.class) && (this.secondArg.getClass() == OperatorVector.class)) {
            res += "\\wedge{";
        }
        else {
            res += "^{";
        }
        res += secondArg.toLatex();
        res += "}";
        return res;
    }
}
