package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorNot extends CalculatorNode {

    public CalculatorNode expr1;

    public OperatorNot(CalculatorNode expr1) {
        super();
        this.expr1 = expr1;
    }

    @Override
    public String print() throws Exception {
        return "not " + this.expr1.print();
    }

    @Override
    public String toLatex() throws Exception {
        return "not " + this.expr1.toLatex();
    }
}
