package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionCplxConj extends CalculatorNode {
    public CalculatorNode value;

    public FunctionCplxConj(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "conj (" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "conj\\left(" + value.toLatex() + "\\right)";
    }

}
