package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OperatorIdentity extends CalculatorNode {

    public CalculatorNode value;

    public OperatorIdentity(CalculatorNode firstArg) {
        super();
        this.value = firstArg;
    }

    @Override
    public String print() throws Exception {
        return "Id(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "I_{" + value.toLatex() + "}";
    }
}
