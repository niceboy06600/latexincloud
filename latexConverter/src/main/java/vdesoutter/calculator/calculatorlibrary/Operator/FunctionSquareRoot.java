package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class FunctionSquareRoot extends CalculatorNode {
    public CalculatorNode value;

    public FunctionSquareRoot(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "\u221A(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\sqrt{" + value.toLatex() + "}";
    }
}
