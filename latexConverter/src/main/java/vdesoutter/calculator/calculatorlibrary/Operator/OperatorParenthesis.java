package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class OperatorParenthesis extends CalculatorNode {

    public CalculatorNode value;

    public OperatorParenthesis(CalculatorNode secondArg) {
        super();
        this.value = secondArg;
    }

    @Override
    public String print() throws Exception {
        return "(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\left(" + value.toLatex() + "\\right)";
    }
}
