package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.List;

public class OperatorFunction extends CalculatorNode {

    public String functionName;
    public List<CalculatorNode> variables;

    public OperatorFunction(String functionName, List<CalculatorNode> variables) {
        super();
        this.functionName = functionName;
        this.variables = variables;
    }

    @Override
    public String print() throws Exception {
        String res = functionName + "(";
        for (int curArgIdx = 0; curArgIdx < variables.size(); ++curArgIdx) {
            if (curArgIdx != 0) {
                res += ",";
            }
            res += variables.get(curArgIdx).print();
        }
        res += ")";

        return res;
    }

    @Override
    public String toLatex() throws Exception {
        String res = functionName + "\\left(";
        for (int curArgIdx = 0; curArgIdx < variables.size(); ++curArgIdx) {
            if (curArgIdx != 0) {
                res += ",";
            }
            res += variables.get(curArgIdx).toLatex();
        }
        res += "\\right)";

        return res;
    }
}
