package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OperatorRecurrence extends CalculatorNode {
    private CalculatorNode recurrence;
    private CalculatorNode variable;
    private List<CalculatorNode> initializer;
    private Map<Integer, CalculatorNode> computedValues;

    public OperatorRecurrence(CalculatorNode recurrence, CalculatorNode variable, List<CalculatorNode> initializer) {
        this.recurrence = recurrence;
        this.initializer = initializer;
        this.variable = variable;
        this.computedValues = new HashMap<>();
    }

    @Override
    public String print() throws Exception {
        String latex = "recur(" + this.recurrence.print() + ", " + this.variable.print() + ", (";
        boolean first = true;
        for (CalculatorNode init : this.initializer) {
            if (! first) {
                latex += ", ";
            }
            else {
                first = false;
            }
            latex += init.print();
        }
        latex += "))";
        return latex;
    }

    @Override
    public String toLatex() throws Exception {
        String latex = this.recurrence.toLatex();
        for (CalculatorNode init : this.initializer) {
            latex += ", ";
            latex += init.toLatex();
        }
        return latex;
    }
}
