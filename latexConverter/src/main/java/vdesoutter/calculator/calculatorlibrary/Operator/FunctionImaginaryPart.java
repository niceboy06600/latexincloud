package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionImaginaryPart extends CalculatorNode {
    public CalculatorNode value;

    public FunctionImaginaryPart(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "Im(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "Im\\left(" + value.toLatex() + "\\right)";
    }

}
