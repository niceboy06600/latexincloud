package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import vdesoutter.calculator.calculatorlibrary.CalculatorNodeContainer;
import vdesoutter.calculator.calculatorlibrary.computation.Pair;

public class OperatorLaplacien extends CalculatorNode {

    public OperatorVariable function;
    public List<CalculatorNode> evaluation;

    public OperatorLaplacien(OperatorVariable function, List<CalculatorNode> values) {
        super();
        this.function = function;
        this.evaluation = values;
    }

    @Override
    public String print() throws Exception {
        String laplString = "\u0394" + function.print();
        laplString += "(";
        for (int argIdx = 0; argIdx < this.evaluation.size(); ++argIdx) {
            if (argIdx != 0) {
                laplString += ",";
            }
            CalculatorNode arg = this.evaluation.get(argIdx);
            laplString += arg.print();
        }
        laplString += ")";
        return laplString;
    }

    @Override
    public String toLatex() throws Exception {
        String laplString = "\\Delta " + function.toLatex();
        laplString += "\\left(";
        for (int argIdx = 0; argIdx < this.evaluation.size(); ++argIdx) {
            if (argIdx != 0) {
                laplString += ",";
            }
            CalculatorNode arg = this.evaluation.get(argIdx);
            laplString += arg.toLatex();
        }
        laplString += "\\right)";
        return laplString;
    }
}
