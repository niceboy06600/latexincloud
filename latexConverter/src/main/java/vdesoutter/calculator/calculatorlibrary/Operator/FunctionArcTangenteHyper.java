package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionArcTangenteHyper extends CalculatorNode {
    public CalculatorNode value;

    public FunctionArcTangenteHyper(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "atanh(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "atanh\\left(" + value.toLatex() + "\\right)";
    }

}
