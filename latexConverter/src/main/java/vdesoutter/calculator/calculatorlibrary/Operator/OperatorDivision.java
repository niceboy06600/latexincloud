package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class OperatorDivision extends CalculatorNode {

    public CalculatorNode firstArg;
    public CalculatorNode secondArg;

    public OperatorDivision(CalculatorNode firstArg, CalculatorNode secondArg) {
        super();
        this.firstArg = firstArg;
        this.secondArg = secondArg;
    }

    @Override
    public String print() throws Exception {
        String res = "";
        res += firstArg.print();
        res += "/";
        res += secondArg.print();
        return res;
    }

    @Override
    public String toLatex() throws Exception {
        return "\\frac{" + firstArg.toLatex() + "}{" + secondArg.toLatex() + "}";
    }
}

