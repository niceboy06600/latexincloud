package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorFactoriel extends CalculatorNode {

    public CalculatorNode expr1;

    public OperatorFactoriel(CalculatorNode expr1) {
        super();
        this.expr1 = expr1;
    }

    @Override
    public String print() throws Exception {
        return this.expr1.print() + "!";
    }

    @Override
    public String toLatex() throws Exception {
        return this.expr1.toLatex() + "!";
    }
}
