package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionArcSinusHyper extends CalculatorNode {
    public CalculatorNode value;

    public FunctionArcSinusHyper(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "asinh(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "asinh\\left(" + value.toLatex() + "\\right)";
    }

}
