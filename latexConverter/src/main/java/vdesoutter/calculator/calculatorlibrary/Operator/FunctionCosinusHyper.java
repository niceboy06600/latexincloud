package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionCosinusHyper extends CalculatorNode {
    public CalculatorNode value;

    public FunctionCosinusHyper(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "cosh(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\cosh\\left(" + value.toLatex() + "\\right)";
    }

}
