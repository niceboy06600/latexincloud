package vdesoutter.calculator.calculatorlibrary.Operator;

public abstract class CalculatorNode {

    public abstract String print() throws Exception;

    public abstract String toLatex() throws Exception;
}
