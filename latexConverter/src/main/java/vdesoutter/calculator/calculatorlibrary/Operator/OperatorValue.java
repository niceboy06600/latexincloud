package vdesoutter.calculator.calculatorlibrary.Operator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class OperatorValue extends CalculatorNode {

    public Double realPart;
    public Double imaginaryPart;

    public OperatorValue(double value) {
        this.realPart = value;
        this.imaginaryPart = 0.0;
    }

    public OperatorValue(double real, double imaginary) {
        this.realPart = real;
        this.imaginaryPart = imaginary;
    }

    public static CalculatorNode createAddComplex(CalculatorNode value1, CalculatorNode value2) {
        if ((value1.getClass() == OperatorValue.class) && (value2.getClass() == OperatorValue.class)) {
            if (((OperatorValue)value1).isReal() && ((OperatorValue)value1).isImaginary()) {
                OperatorValue res = new OperatorValue(0);
                add(res, (OperatorValue) value1);
                add(res, (OperatorValue) value2);
                return res;
            }
            if (((OperatorValue)value1).isImaginary() && ((OperatorValue)value1).isReal()) {
                OperatorValue res = new OperatorValue(0);
                add(res, (OperatorValue) value1);
                add(res, (OperatorValue) value2);
                return res;
            }
        }
        return new OperatorPlus(value1, value2);
    }

    public static CalculatorNode createSubstComplex(CalculatorNode value1, CalculatorNode value2) {
        if ((value1.getClass() == OperatorValue.class) && (value2.getClass() == OperatorValue.class)) {
            if (((OperatorValue)value1).isReal() && ((OperatorValue)value1).isImaginary()) {
                OperatorValue res = new OperatorValue(0);
                add(res, (OperatorValue) value1);
                substr(res, (OperatorValue) value2);
                return res;
            }
            if (((OperatorValue)value1).isImaginary() && ((OperatorValue)value1).isReal()) {
                OperatorValue res = new OperatorValue(0);
                add(res, (OperatorValue) value1);
                substr(res, (OperatorValue) value2);
                return res;
            }
        }
        return new OperatorMoins(value1, value2);
    }

    public static CalculatorNode createMultComplex(CalculatorNode value1, CalculatorNode value2) {
        if ((value1.getClass() == OperatorValue.class) && (value2.getClass() == OperatorValue.class)) {
            if (((OperatorValue)value1).isReal() && ((OperatorValue)value2).isImaginary() && (((OperatorValue)value2).imaginaryPart == 1)) {
                OperatorValue res = new OperatorValue(0, ((OperatorValue)value1).realPart);
                return res;
            }
            if (((OperatorValue)value1).isImaginary() && ((OperatorValue)value2).isReal() && (((OperatorValue)value1).imaginaryPart == 1)) {
                OperatorValue res = new OperatorValue(0, ((OperatorValue)value2).realPart);
                return res;
            }
        }
        return new OperatorMultiplication(value1, value2);
    }

    public static void add(OperatorValue coefficient, OperatorValue coefficient1) {
        coefficient.realPart += coefficient1.realPart;
        coefficient.imaginaryPart += coefficient1.imaginaryPart;
    }

    public static void substr(OperatorValue coefficient, OperatorValue coefficient1) {
        coefficient.realPart -= coefficient1.realPart;
        coefficient.imaginaryPart -= coefficient1.imaginaryPart;
    }

    public static void mult(OperatorValue coefficient, OperatorValue operatorValue) {
        double real = coefficient.realPart * operatorValue.realPart - coefficient.imaginaryPart * operatorValue.imaginaryPart;
        double imag = coefficient.realPart * operatorValue.imaginaryPart + coefficient.imaginaryPart * operatorValue.realPart;
        coefficient.realPart = real;
        coefficient.imaginaryPart = imag;
    }

    public static void divide(OperatorValue coefficient, OperatorValue operatorValue) {
        double real = coefficient.realPart * operatorValue.realPart + coefficient.imaginaryPart * operatorValue.imaginaryPart;
        double imag = coefficient.imaginaryPart * operatorValue.realPart - coefficient.realPart * operatorValue.imaginaryPart;
        double denominator = operatorValue.realPart * operatorValue.realPart + operatorValue.imaginaryPart * operatorValue.imaginaryPart;
        coefficient.realPart = real / denominator;
        coefficient.imaginaryPart = imag / denominator;
    }


    public double getRealPart() {
        return realPart;
    }

    public double getImaginaryPart() {
        return imaginaryPart;
    }

    @Override
    public String print() {
        String res = "";
        if (!this.isImaginary()) {
            if (realPart == realPart.intValue()) {
                res += String.format("%d", realPart.intValue());
            } else {
                DecimalFormat df = new DecimalFormat("###.####");
                DecimalFormatSymbols decimalSymbol = new DecimalFormatSymbols(Locale.getDefault());
                decimalSymbol.setDecimalSeparator('.');
                df.setGroupingUsed(false);
                df.setDecimalFormatSymbols(decimalSymbol);
                res += df.format(realPart);
            }
        }
        if (!this.isReal()) {
            if (!res.isEmpty() && this.getImaginaryPart() > 0) {
                res += "+";
            }
            if ((imaginaryPart != 1) && (imaginaryPart != -1)) {
                if (imaginaryPart == imaginaryPart.intValue()) {
                    res += String.format("%d", imaginaryPart.intValue());
                } else {
                    DecimalFormat df = new DecimalFormat("###.####");
                    DecimalFormatSymbols decimalSymbol = new DecimalFormatSymbols(Locale.getDefault());
                    decimalSymbol.setDecimalSeparator('.');
                    df.setGroupingUsed(false);
                    df.setDecimalFormatSymbols(decimalSymbol);
                    res += df.format(imaginaryPart);
                }
                res += "*i";
            }
            else if (imaginaryPart == 1) {
                res += "i";
            }
            else if (imaginaryPart == -1) {
                res += "-i";
            }
        }
        if (this.isNull()) {
            res = "0";
        }
        return res;
    }

    @Override
    public String toLatex() {
        String res = "";
        if (!this.isImaginary()) {
            if (realPart == realPart.intValue()) {
                res += String.format("%d", realPart.intValue());
            } else {
                DecimalFormat df = new DecimalFormat("###.####");
                DecimalFormatSymbols decimalSymbol = new DecimalFormatSymbols(Locale.getDefault());
                decimalSymbol.setDecimalSeparator('.');
                df.setGroupingUsed(false);
                df.setDecimalFormatSymbols(decimalSymbol);
                res += df.format(realPart);
            }
        }
        if (!this.isReal()) {
            if (!res.isEmpty() && this.getImaginaryPart() > 0) {
                res += "+";
            }
            if ((imaginaryPart != 1) && (imaginaryPart != -1)) {
                if (imaginaryPart == imaginaryPart.intValue()) {
                    res += String.format("%d", imaginaryPart.intValue());
                } else {
                    DecimalFormat df = new DecimalFormat("###.####");
                    DecimalFormatSymbols decimalSymbol = new DecimalFormatSymbols(Locale.getDefault());
                    decimalSymbol.setDecimalSeparator('.');
                    df.setGroupingUsed(false);
                    df.setDecimalFormatSymbols(decimalSymbol);
                    res += df.format(imaginaryPart);
                }
                res += "*i";
            } else if (imaginaryPart == 1) {
                res += "i";
            } else if (imaginaryPart == -1) {
                res += "-i";
            }
        }
        if (this.isNull()) {
            res = "0";
        }
        return res;
    }

    public boolean isImaginary() {
        return this.realPart == 0;
    }

    public boolean isReal() {
        return this.imaginaryPart == 0;
    }

    public boolean isNull() {
        return this.isImaginary() && this.isReal();
    }

    public boolean isOne() {
        return this.realPart == 1 && this.isReal();
    }

    public boolean isMinusOne() {
        return this.realPart == -1 && this.isReal();
    }
}
