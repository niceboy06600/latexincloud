package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

import vdesoutter.calculator.calculatorlibrary.CalculatorNodeContainer;

public class FunctionTangente extends CalculatorNode {
    public CalculatorNode value;

    public FunctionTangente(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "tan(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\tan\\left(" + value.toLatex() + "\\right)";
    }

}
