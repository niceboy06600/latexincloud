package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class FunctionSinusHyper extends CalculatorNode {
    public CalculatorNode value;

    public FunctionSinusHyper(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "sinh(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\sinh\\left(" + value.toLatex() + "\\right)";
    }

}
