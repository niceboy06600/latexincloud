package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionArcSinus extends CalculatorNode {
    public CalculatorNode value;

    public FunctionArcSinus(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "asin(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\arcsin\\left(" + value.toLatex() + "\\right)";
    }
}
