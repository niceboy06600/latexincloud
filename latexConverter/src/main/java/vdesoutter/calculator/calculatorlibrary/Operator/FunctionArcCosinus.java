package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionArcCosinus extends CalculatorNode {
    public CalculatorNode value;

    public FunctionArcCosinus(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "acos(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\arccos\\left(" + value.toLatex() + "\\right)";
    }

}
