package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OperatorAverage extends CalculatorNode {

    public List<CalculatorNode> values;

    public OperatorAverage(List<CalculatorNode> secondArg) {
        super();
        this.values = secondArg;
    }

    @Override
    public String print() throws Exception {
        String res = "avg(";
        for (int curArgIdx = 0; curArgIdx < values.size(); ++curArgIdx) {
            if (curArgIdx != 0) {
                res += ",";
            }
            res += values.get(curArgIdx).print();
        }
        res += ")";

        return res;
    }

    @Override
    public String toLatex() throws Exception {
        String res = "\\overline{\\left(";
        for (int curArgIdx = 0; curArgIdx < values.size(); ++curArgIdx) {
            if (curArgIdx != 0) {
                res += ",";
            }
            res += values.get(curArgIdx).print();
        }
        res += "\\right)}";

        return res;
    }
}
