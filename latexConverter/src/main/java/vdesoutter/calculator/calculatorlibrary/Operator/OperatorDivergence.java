package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import vdesoutter.calculator.calculatorlibrary.CalculatorNodeContainer;

public class OperatorDivergence extends CalculatorNode {

    public CalculatorNode function;
    public List<CalculatorNode> evaluation;

    public OperatorDivergence(CalculatorNode function, List<CalculatorNode> values) throws Exception {
        super();
        this.function = function;
        this.evaluation = values;
    }

    @Override
    public String print() throws Exception {
        String laplString = "div " + function.print() + "(";
        for (int argIdx = 0; argIdx < this.evaluation.size(); ++argIdx) {
            if (argIdx != 0) {
                laplString += ",";
            }
            CalculatorNode arg = this.evaluation.get(argIdx);
            laplString += arg.print();
        }
        laplString += ")";
        return laplString;
    }

    @Override
    public String toLatex() throws Exception {
        String laplString = "div " + function.toLatex() + "\\left(";
        for (int argIdx = 0; argIdx < this.evaluation.size(); ++argIdx) {
            if (argIdx != 0) {
                laplString += ",";
            }
            CalculatorNode arg = this.evaluation.get(argIdx);
            laplString += arg.toLatex();
        }
        laplString += "\\right)";
        return laplString;
    }
}
