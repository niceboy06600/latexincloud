package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorEqual extends CalculatorNode {

    public CalculatorNode function;
    public CalculatorNode value;

    public OperatorEqual(CalculatorNode firstArg, CalculatorNode secondArg) {
        super();
        this.function = firstArg;
        this.value = secondArg;
    }

    @Override
    public String print() throws Exception {
        if (value != null) {
            return function.print() + "=" + value.print();
        }
        else {
            return function.print();
        }
    }

    @Override
    public String toLatex() throws Exception {
        return function.toLatex() + "=" + value.toLatex();
    }
}
