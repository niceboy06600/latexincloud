package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OperatorEquaDiff extends CalculatorNode {
    public CalculatorNode recurrence;
    public CalculatorNode variable;
    public List<CalculatorNode> initializer;

    public OperatorEquaDiff(CalculatorNode recurrence, CalculatorNode variable, List<CalculatorNode> initializer) {
        this.recurrence = recurrence;
        this.initializer = initializer;
        this.variable = variable;
    }

    @Override
    public String print() throws Exception {
        String latex = "equa_diff(" + this.recurrence.print() + ", " + this.variable.print() + ", (";
        boolean first = true;
        for (CalculatorNode init : this.initializer) {
            if (! first) {
                latex += ", ";
            }
            else {
                first = false;
            }
            latex += init.print();
        }
        latex += "))";
        return latex;
    }

    @Override
    public String toLatex() throws Exception {
        String latex = "\\left\\{ \\begin{array}{l} " + this.recurrence.toLatex() + " \\\\ ";
        boolean first = true;
        for (CalculatorNode init : this.initializer) {
            if (! first) {
                latex += " \\\\ ";
            }
            else {
                first = false;
            }
            latex += init.toLatex();
        }
        latex += "\\end{array} \\right.";
        return latex;
    }
}
