package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vdesoutter.calculator.calculatorlibrary.CalculatorNodeContainer;
import vdesoutter.calculator.calculatorlibrary.computation.Pair;

public class OperatorGradient extends CalculatorNode {

    public CalculatorNode function;
    public List<CalculatorNode> evaluation;

    public OperatorGradient(CalculatorNode function, List<CalculatorNode> values) throws Exception {
        super();
        this.function = function;
        this.evaluation = values;
    }

    @Override
    public String print() throws Exception {
        String laplString = "\u2207" + function.print();
        laplString += "(";
        for (int argIdx = 0; argIdx < this.evaluation.size(); ++argIdx) {
            if (argIdx != 0) {
                laplString += ",";
            }
            CalculatorNode arg = this.evaluation.get(argIdx);
            laplString += arg.print();
        }
        laplString += ")";
        return laplString;
    }

    @Override
    public String toLatex() throws Exception {
        String laplString = "\\nabla " + function.toLatex();
        laplString += "\\left(";
        for (int argIdx = 0; argIdx < this.evaluation.size(); ++argIdx) {
            if (argIdx != 0) {
                laplString += ",";
            }
            CalculatorNode arg = this.evaluation.get(argIdx);
            laplString += arg.toLatex();
        }
        laplString += "\\right)";
        return laplString;
    }
}
