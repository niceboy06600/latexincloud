package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class FunctionTangenteHyper extends CalculatorNode {
    public CalculatorNode value;

    public FunctionTangenteHyper(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "tanh(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\tanh\\left(" + value.toLatex() + "\\right)";
    }

}
