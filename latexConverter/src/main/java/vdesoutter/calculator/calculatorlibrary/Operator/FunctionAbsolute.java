package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionAbsolute extends CalculatorNode {
    public CalculatorNode value;

    public FunctionAbsolute(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "abs(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\left|" + value.toLatex() + "\\right|";
    }
}
