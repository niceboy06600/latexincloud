package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.HashMap;
import java.util.Map;

public class OperatorProduct extends CalculatorNode {

    public CalculatorNode function;
    public CalculatorNode minValue;
    public CalculatorNode maxValue;
    public OperatorVariable variable;

    public OperatorProduct(CalculatorNode function, OperatorVariable variable, CalculatorNode minBound, CalculatorNode maxBound) {
        super();
        this.function = function;
        this.variable = variable;
        this.minValue = minBound;
        this.maxValue = maxBound;
    }

    @Override
    public String print() throws Exception {
        return "\u03A0(" + function.print() + "," + variable.value + "," + minValue.print() + "," + maxValue.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\prod_{" + variable.value + "=" + minValue.toLatex() + "}^{" + variable.value + "=" + maxValue.toLatex() + "}" + function.toLatex();
    }
}
