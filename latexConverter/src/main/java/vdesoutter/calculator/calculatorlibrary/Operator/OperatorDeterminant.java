package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorDeterminant extends CalculatorNode {

    public CalculatorNode firstArg;

    public OperatorDeterminant(CalculatorNode firstArg) {
        super();
        this.firstArg = firstArg;
    }

    @Override
    public String print() throws Exception {
        return "det(" + firstArg.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "det " + firstArg.toLatex() + "";
    }
}
