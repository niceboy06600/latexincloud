package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.HashMap;
import java.util.Map;

public class OperatorIf extends CalculatorNode {
    CalculatorNode condition;
    CalculatorNode resultThen;
    CalculatorNode resultElse;

    public OperatorIf(CalculatorNode ifNode, CalculatorNode thenNode, CalculatorNode elseNode) {
        condition = ifNode;
        resultThen = thenNode;
        resultElse = elseNode;
    }

    @Override
    public String print() throws Exception {
        return "if " + this.condition.print() + " then " + this.resultThen.print() + " else " + this.resultElse.print();
    }

    @Override
    public String toLatex() throws Exception {
        return "if " + this.condition.toLatex() + " then " + this.resultThen.toLatex() + " else " + this.resultElse.toLatex();
    }
}
