package vdesoutter.calculator.calculatorlibrary.Operator;


import java.util.ArrayList;
import java.util.List;

import vdesoutter.calculator.calculatorlibrary.computation.Pair;

public class OperatorDerivate extends CalculatorNode {

    public OperatorVariable function;
    public List<Pair<OperatorVariable, Integer>> order;
    public List<CalculatorNode> values;

    public OperatorDerivate(OperatorVariable function, Integer order, List<CalculatorNode> values) {
        this.function = function;
        this.order = new ArrayList<Pair<OperatorVariable, Integer>>();
        this.order.add(new Pair<OperatorVariable, Integer>(null, order));
        this.values = values;
    }

    public OperatorDerivate(OperatorVariable function, List<Pair<OperatorVariable, Integer>> order, List<CalculatorNode> values) {
        this.function = function;
        this.order = order;
        this.values = values;
    }

    @Override
    public String print() throws Exception {
        String derString = operator();
        derString += "(";
        for (int argIdx = 0; argIdx < this.values.size(); ++argIdx) {
            if (argIdx != 0) {
                derString += ",";
            }
            CalculatorNode arg = this.values.get(argIdx);
            derString += arg.print();
        }
        derString += ")";
        return derString;
    }

    public String operator() throws Exception {
        String derString = "";
        Pair<OperatorVariable, Integer> oneOrder;
        if (this.order.size() == 1) {
            oneOrder = this.order.get(0);
            if (oneOrder.first == null) {
                if (oneOrder.second == 1) {
                    derString += this.function.print() + "'";
                } else if (oneOrder.second == 2) {
                    derString += this.function.print() + "\"";
                }
            } else {
                if (oneOrder.second == 1) {
                    derString += "\u2202" + this.function.print() + "/" + "\u2202" + oneOrder.first.value;
                } else {
                    derString += "\u2202^" + oneOrder.second + " " + this.function.print() + "/" + "\u2202" + oneOrder.first.value + "^" + oneOrder.second;
                }
            }
        } else {
            int derTotal = 0;
            for (Pair<OperatorVariable, Integer> curOrder : this.order) {
                derTotal += curOrder.second;
            }
            derString += "\u2202^" + derTotal + " " + this.function.print() + "/";
            for (Pair<OperatorVariable, Integer> curOrder : this.order) {
                if (curOrder.second == 1) {
                    derString += "\u2202" + curOrder.first.value;
                } else {
                    derString += "\u2202" + curOrder.first.value + "^" + curOrder.second;
                }
            }
        }
        return derString;
    }

    @Override
    public String toLatex() throws Exception {
        String derString = "";
        Pair<OperatorVariable, Integer> oneOrder;
        if (this.order.size() == 1) {
            oneOrder = this.order.get(0);
            if (oneOrder.first == null) {
                if (oneOrder.second == 1) {
                    derString += this.function.toLatex() + "'";
                } else if (oneOrder.second == 2) {
                    derString += this.function.toLatex() + "''";
                }
            } else {
                if (oneOrder.second == 1) {
                    derString += "\\frac{\\partial " + this.function.toLatex() + "}{" + "\\partial " + oneOrder.first.value + " }";
                } else {
                    derString += "\\frac{\\partial^" + oneOrder.second + " " + this.function.toLatex() + "}{" + "\\partial" + oneOrder.first.value + "^" + oneOrder.second + "}";
                }
            }
        } else {
            int derTotal = 0;
            for (Pair<OperatorVariable, Integer> curOrder : this.order) {
                derTotal += curOrder.second;
            }
            derString += "\\frac{\\partial ^{" + derTotal + "} " + this.function.toLatex() + "}{";
            for (Pair<OperatorVariable, Integer> curOrder : this.order) {
                if (curOrder.second == 1) {
                    derString += "\\partial " + curOrder.first.value;
                } else {
                    derString += "\\partial " + curOrder.first.value + "^" + curOrder.second;
                }
            }
            derString += "}";
        }
        derString += "\\left(";
        for (int argIdx = 0; argIdx < this.values.size(); ++argIdx) {
            if (argIdx != 0) {
                derString += ",";
            }
            CalculatorNode arg = this.values.get(argIdx);
            derString += arg.toLatex();
        }
        derString += "\\right)";
        return derString;
    }
}
