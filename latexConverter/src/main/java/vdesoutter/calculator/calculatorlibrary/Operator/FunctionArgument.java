package vdesoutter.calculator.calculatorlibrary.Operator;

public class FunctionArgument extends CalculatorNode {
    public CalculatorNode value;

    public FunctionArgument(CalculatorNode value) {
        super();
        this.value = value;
    }

    @Override
    public String print() throws Exception {
        return "arg(" + value.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "arg\\left(" + value.toLatex() + "\\right)";
    }

}
