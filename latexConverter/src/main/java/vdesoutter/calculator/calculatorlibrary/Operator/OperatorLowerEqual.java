package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class OperatorLowerEqual extends CalculatorNode {
    public CalculatorNode expr1;
    public CalculatorNode expr2;

    public OperatorLowerEqual(CalculatorNode expr1, CalculatorNode expr2) {
        super();
        this.expr1 = expr1;
        this.expr2 = expr2;
    }

    @Override
    public String print() throws Exception {
        return this.expr1.print() + " \u2264 " + this.expr2.print();
    }

    @Override
    public String toLatex() throws Exception {
        return this.expr1.toLatex() + " \\le " + this.expr2.toLatex();
    }
}
