package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.Map;

public class OperatorAssign extends CalculatorNode {

    public CalculatorNode function;
    public CalculatorNode value;

    public OperatorAssign(CalculatorNode firstArg, CalculatorNode secondArg) {
        super();
        this.function = firstArg;
        this.value = secondArg;
    }

    @Override
    public String print() throws Exception {
        return function.print() + "=" + value.print();
    }

    @Override
    public String toLatex() throws Exception {
        return function.toLatex() + "=" + value.toLatex();
    }
}
