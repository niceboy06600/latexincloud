package vdesoutter.calculator.calculatorlibrary.Operator;

public class OperatorEgality extends CalculatorNode {

    public CalculatorNode expr1;
    public CalculatorNode expr2;

    public OperatorEgality(CalculatorNode expr1, CalculatorNode expr2) {
        super();
        this.expr1 = expr1;
        this.expr2 = expr2;
    }

    @Override
    public String print() throws Exception {
        return this.expr1.print() + " == " + this.expr2.print();
    }

    @Override
    public String toLatex() throws Exception {
        return this.expr1.toLatex() + " == " + this.expr2.toLatex();
    }
}
