package vdesoutter.calculator.calculatorlibrary.Operator;

import java.util.HashMap;
import java.util.Map;

public class OperatorSummation extends CalculatorNode {

    public CalculatorNode function;
    public CalculatorNode minValue;
    public CalculatorNode maxValue;
    public OperatorVariable variable;

    public OperatorSummation(CalculatorNode function, OperatorVariable variable, CalculatorNode minBound, CalculatorNode maxBound) {
        super();
        this.function = function;
        this.variable = variable;
        this.minValue = minBound;
        this.maxValue = maxBound;
    }

    @Override
    public String print() throws Exception {
        return "\u2211(" + function.print() + "," + variable.value + "," + minValue.print() + "," + maxValue.print() + ")";
    }

    @Override
    public String toLatex() throws Exception {
        return "\\sum_{" + variable.value + "=" + minValue.print() + "}^{" + variable.value + "=" + maxValue.print() + "}" + function.toLatex();
    }
}
