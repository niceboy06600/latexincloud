package vdesoutter.calculator.calculatorlibrary.computation;

import vdesoutter.calculator.calculatorlibrary.Operator.CalculatorNode;

public class Pair<First, Second> {
    public First first;
    public Second second;
    public CalculatorNode third;

    public Pair(First first, Second second) {
        this.first = first;
        this.second = second;
        this.third = (CalculatorNode) first;
    }
}