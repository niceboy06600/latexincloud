package vdesoutter.calculator.calculatorlibrary.Matrix;

/**
 * Created by Vincent on 28/01/2018.
 */

public class MatrixSize {
    public int sizeX;
    public int sizeY;

    public MatrixSize(int X, int Y) {
        this.sizeX = X;
        this.sizeY = Y;
    }

    public boolean isSquare() {
        return (this.sizeX == this.sizeY);
    }

    @Override
    public boolean equals(Object obj) {
        return (((MatrixSize) obj).sizeX == this.sizeX) && (((MatrixSize) obj).sizeY == this.sizeY);
    }

    public String print() {
        return "(" + sizeX + "," + sizeY + ")";
    }
}
