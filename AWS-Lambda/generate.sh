cd /tmp
echo $1 >> decode.log
decoded=`echo $1 | base64 --decode`
echo ${decoded} >> decode.log
sed "s/##EXPRESSION##/${decoded}/g" < /var/task/math_template.tex > math.tex
sed -i 's/\\x26/\&/g' math.tex
latex math.tex
dvipdf math.dvi 
/var/task/pdfcrop.pl --margins "5 5 5 5" math.pdf
convert math-crop.pdf math.jpg
