package vdesoutter.app;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.codec.binary.Base64;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class LatexGenerator implements RequestHandler<Map<String, String>, String> {

    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Override
    public String handleRequest(Map<String, String> event, Context context) {
        LambdaLogger logger = context.getLogger();
        try {
            String response = new String("200 OK " + event.get("value"));
            String sCommandString = "/var/task/generate.sh " + new String(Base64.encodeBase64(event.get("value").getBytes(StandardCharsets.UTF_8)));
            logger.log("Command for latex : " + sCommandString);
            CommandLine oCmdLine = CommandLine.parse(sCommandString);
            DefaultExecutor oDefaultExecutor = new DefaultExecutor();
            oDefaultExecutor.setExitValue(0);

            oDefaultExecutor.execute(oCmdLine);

            File file = new File("/tmp/math.jpg");
            byte[] encoded = new byte[0];

            encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
            return new String(encoded, StandardCharsets.US_ASCII);
        } catch (ExecuteException e) {
            logger.log("Execution failed.");
            e.printStackTrace();
        } catch (IOException e) {
            logger.log("permission denied.");
            e.printStackTrace();
        }
        return "400 KO";
    }
}
