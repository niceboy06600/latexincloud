package vdesoutter.app;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.google.gson.Gson;

import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class AwsCaller {

    public static void main(String [] args) {

        System.out.println(args[0]);

        AWSCredentials credentials = DefaultAWSCredentialsProviderChain.getInstance().getCredentials();

        String functionName = "arn:aws:lambda:eu-west-1:931444911850:function:test_docker";

        //This will convert object to JSON String
        Map<String, String> values = new HashMap<>();

        values.put("value", args[0].replace("\\", "\\\\"));
        String inputJSON = new Gson().toJson(values);
        InvokeRequest lmbRequest = new InvokeRequest()
                .withFunctionName(functionName)
                .withPayload(inputJSON);

        lmbRequest.setInvocationType(InvocationType.RequestResponse);

        AWSLambda lambda = AWSLambdaClientBuilder.standard()
                .withRegion(Regions.EU_WEST_1)
                .withCredentials(new AWSStaticCredentialsProvider(credentials)).build();

        InvokeResult lmbResult = lambda.invoke(lmbRequest);

        String resultJSON = new String(lmbResult.getPayload().array(), Charset.forName("UTF-8"));

        System.out.println(resultJSON);

        byte[] decodedImg = Base64.decodeBase64(resultJSON.getBytes(StandardCharsets.UTF_8));
        Path destinationFile = Paths.get("./", "myImage.jpg");
        try {
            Files.write(destinationFile, decodedImg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
